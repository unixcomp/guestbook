require 'rvm/capistrano'
require 'bundler/capistrano'

set :application, "guestbook"
set :repository,  "git://gitorious.org/unixcomp-net/guestbook.git"
set :domain,      "rusoc.unixcomp.org"

set :scm, :git 
set :scm_url, "#{repository}"
set :branch,     'master'

role :web, "#{domain}"
role :app, "#{domain}"
role :db,  "#{domain}", :primary => true

set :user, 'deployer'
set :port, 3389
set :use_sudo, false

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :rvm_type, :user
set :rvm_ruby_string, 'default'

set :deploy_via, :remote_cache
set :keep_releases, '3'

set :app_dir, "/home/#{user}/apps/#{application}"
set :deploy_to, "#{app_dir}/deploy"

# if you want to clean up old releases on each deploy uncomment this:
after "deploy", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  task :setup_config, :roles => :app do
    run "mkdir -p #{shared_path}/config"
    put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    put File.read("config/application.yml"), "#{shared_path}/config/application.yml"
    puts "Now edit the config files in #{shared_path}."
  end
  after "deploy:setup", "deploy:setup_config"

  task :reset_config, :roles => :app do
    run "rm -rf #{shared_path}/config"
    deploy.setup_config
  end

  task :symlink_config, :roles => :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

  desc "Make sure local git is in sync with remote."
  task :check_revision, :roles => :web do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end
  before "deploy", "deploy:check_revision"

end
