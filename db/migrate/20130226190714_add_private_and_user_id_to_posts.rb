class AddPrivateAndUserIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :private, :boolean, :default => false, :null => false
    add_column :posts, :user_id, :integer, :default => 0, :null => false

    add_index :posts, :private
    add_index :posts, :user_id
  end
end
