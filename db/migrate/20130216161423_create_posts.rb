class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :username, :null => false
      t.string :email, :null => false
      t.text :content, :null => false
      t.boolean :approved, :default => false, :null => false

      t.timestamps
    end
    add_index :posts, :approved
    add_index :posts, :created_at
    add_index :posts, :updated_at
  end
end
