require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest

  test "should route to posts feed" do
    assert_recognizes({:controller => "posts", :action => "index", :format => "atom"}, 'feed')
  end

  test "should route to user and session" do
    assert_recognizes({:controller => "users", :action => "new"}, 'signup')
    assert_recognizes({:controller => "sessions", :action => "new"}, 'login')
    assert_recognizes({:controller => "sessions", :action => "destroy"}, 'logout')
  end

end
