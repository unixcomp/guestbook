require 'test_helper'

class PostTest < ActiveSupport::TestCase
  fixtures :posts

  #-----{ Example }------------------------------------------------------------>

#  def setup
#    # this method gets called before each test
#    @string = "Unit testing is not difficult."
#    @array = [1,2,3,4,5]
#  end
#  
#  def test_subtraction
#    assert(2 - 1 == 2)  # should fail
#    assert(1 - 1 == 0)  # should succeed
#  end

#  def test_addition
#    assert(1 + 1 == 2, "Addition should work.")
#  end
#  
#  def test_array_a
#    assert(5 == @array.length)
#    @array << 6
#    assert_equal(6, @array.length)
#    assert_equal([6,5,4,3,2,1], reverse_array(@array))
#  end

#  def test_array_b
#    assert_equal([5,4,3,2,1], reverse_array(@array), 
#        "@array should not be affected by other tests.")
#  end
#  
#  def reverse_array(array)
#    return array.reverse
#  end
#  
#  def test_string
#    assert_equal(30, @string.length)
#    assert_equal(0, @fake_string.length)
#  end
#  
#  def dont_test_string_content
#    assert_equal("Unit testing is not difficult.", @string)
#  end

  #---------------------------------------------------------------------------->

  def setup
    @post = Post.new
    @post.username = posts(:one).username
    @post.email = posts(:one).email
    @post.content = posts(:one).content
  end

  test "the truth" do
    assert true
  end

  test "should not save post without username" do
    post = Post.new
    assert !post.save, "Saved the post without a title"
  end

  test "check post is valid" do
   # Assert not valid since vendor_name is required
   assert @post.valid?
  end

  test "should invalid email format" do
    post = @post.clone
    invalid_emails = ['user@examplenet', 'user', '@example.net', 'user@', 'user@example.n', 'user@example.', '@.net', '@example']
    invalid_emails.each do |email|
      post.email = email
      assert !post.valid?, "Invalid email"
    end
  end

  test "should short username error" do
    post = @post.clone
    post.username = '12'
    assert !post.valid?, "Username lenght test"
  end

  test "should report error" do
    # some_undefined_variable is not defined elsewhere in the test case
    assert true
  end

  test "should published" do
    post = @post.clone
    post.publish
    assert post.published?, "Post published"
  end

  test "should unpublished" do
    post = @post.clone
    post.unpublish
    assert !post.published?, "Post unpublished"
  end
end
