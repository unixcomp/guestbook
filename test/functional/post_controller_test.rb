require 'test_helper'

class PostControllerTest < ActionController::TestCase
  fixtures :posts
  
  def setup
    @controller = PostsController.new
    @post = Post.first
  end 

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:posts)
  end

  test "should get index atom feed" do
    get :index, :format => :atom
    assert_response :success
    assert_not_nil assigns(:posts)
  end

  test "should get edit" do
    get :edit,  {:id => @post.id}
    assert_response :success
    assert_not_nil assigns(:post)
  end

  test "should get show" do
    get :show,  {:id => @post.id}
    assert_response :success
    assert_not_nil assigns(:post)
    assert_select 'div.post-content', @post.content
  end

  test "should get new" do
    get :new
    assert_response :success
    assert_not_nil assigns(:post)
  end

  test "should create post" do
    assert_difference('Post.count') do
      post :create, :post => { :username => posts(:one).username, :content => posts(:one).content, :email => posts(:one).email}
    end
    assert_redirected_to post_path(assigns(:post))
  end

  test "should update post" do
    put :update, { :id => @post.id, :content => posts(:one).content}
    assert_redirected_to post_path(assigns(:post))
  end

end
