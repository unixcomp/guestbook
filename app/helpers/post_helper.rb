module PostHelper
  def markdown(text, without_follow = false)
    if without_follow
      markdown = Redcarpet::Markdown.new(ApplicationHelper::MHTMLwithoutFollow)
    else
      markdown = Redcarpet::Markdown.new(ApplicationHelper::MHTML)
    end
    return markdown.render(text).html_safe
  end

  def post_css_classes(post, classes = [])
    if !post.approved?
      classes.push 'not-approved'
    end
    if post.private?
      classes.push 'private'
    end
    classes
  end
end
