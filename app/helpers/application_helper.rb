# -*- encoding: utf-8 -*-
module ApplicationHelper
  class MHTMLwithoutFollow < Redcarpet::Render::HTML
    def initialize(options={})
      super options.merge(
        :hard_wrap => true,
        :filter_html => true,
        :safe_links_only => true
      )
    end

    def link(link, title, content)
      "<a href='#{link}' #{"title='#{title}' " unless title.blank?}rel='nofollow'>#{content}</a>"
    end
  end

  class MHTML < Redcarpet::Render::HTML
    def initialize(options={})
      super options.merge(
        :hard_wrap => true,
        :filter_html => true,
        :safe_links_only => true
      )
    end
  end

  def check_markup(html)
    tags = %w(strong i ul li em dt dd p br a pre code blockquote sub sup ol mark)
    attrs = %w(href rel title)
    sanitize html, :tags => tags, :attributes => attrs
  end

  def mark_required(object, attribute)
    if object.class.validators_on(attribute).map(&:class).include? ActiveModel::Validations::PresenceValidator
      '<span class="form-required" title="Обязательно для заполнения.">*</span>'.html_safe
    end 
  end
end
