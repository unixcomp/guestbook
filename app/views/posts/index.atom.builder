atom_feed do |feed|
  feed.title("Rusoc guestbook")
  feed.updated(@posts[0].created_at) if @posts.length > 0

  @posts.each do |post|
    feed.entry post do |entry|
      entry.title truncate(strip_tags(markdown post.content, true), :length => 30)
      entry.content check_markup(markdown post.content, true), :type => 'html'

      entry.author do |author|
        author.name(strip_tags post.username)
      end
    end
  end
end
