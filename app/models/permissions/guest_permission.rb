module Permissions
  class GuestPermission < BasePermission
    def initialize
      allow :users, [:new, :create]
      allow :sessions, [:new, :create, :destroy]
      allow :posts, [:index, :show, :new, :create]
      allow :password_resets, [:new, :create, :edit, :update]
      allow_param :post, [:username, :private, :content, :email]
      allow_param :user, [:password, :password_confirmation, :email]
    end
  end
end
