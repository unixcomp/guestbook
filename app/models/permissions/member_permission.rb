module Permissions
  class MemberPermission < BasePermission
    def initialize(user)
      allow :sessions, [:destroy]
      allow :posts, [:index, :show, :new, :create]
      allow_param :post, [:username, :private, :content] 
    end
  end
end
