class Post < ActiveRecord::Base
  belongs_to :user

  attr_accessible :content, :email, :username, :approved, :private

  validates :username, :content, :email, :presence => true
  validates :username, :length => { :minimum => 3 }
  validates :email, :email_format => true

  scope :published, where(:approved => true)
  scope :open, where(:approved => true, :private => false)

  def self.search(search)
    if search
      where('content ILIKE ?', "%#{search}%")
    else
      scoped
    end
  end

  def self.last_post_for_email(email)
    unscoped.select("username").where("email = ?", email).last
  end

  # Публикует
  def publish
    self.approved = true
  end

  # Снимает с публикации
  def unpublish
    self.approved = false
  end

  # Возвращает признак публикации
  def published?
    self.approved
  end
end
