class UserMailer < ActionMailer::Base
  default :from => "noreply@rusoc.unixcomp.org",
          :sender => 'mail@mail.unixcomp.org'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    mail :to => user.email,
      :sender => 'mail@mail.unixcomp.org',
      :subject => "Password Reset"
  end
end
