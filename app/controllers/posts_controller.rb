# -*- encoding: utf-8 -*-
class PostsController < ApplicationController

  # GET /posts
  # GET /posts.json
  def index
    if logged_in? && current_user.admin?
      @posts = Post.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(10)
    elsif logged_in?
      @posts = Post.published.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(10)
    else
      @posts = Post.open.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(10)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.atom
      #format.json { render :json => @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = current_resource

    respond_to do |format|
      format.html # show.html.erb
      #format.json { render :json => @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new

    if logged_in?
      last_post = Post.last_post_for_email(current_user.email)
      if last_post
        @post.username = last_post.username
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      #format.json { render :json => @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = current_resource
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post])

    if logged_in?
      @post.email = current_user.email
      @post.user_id = current_user.id
    end

    respond_to do |format|
      if ( logged_in? || verify_recaptcha(:model => @post)) && @post.save
        format.html { redirect_to posts_path, :notice => success_message(@post) }
        #format.json { render :json => @post, :status => :created, :location => @post }
      else
        flash.clear
        format.html { render :action => "new" }
        #format.json { render :json => @post.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = current_resource

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, :notice => 'Сообщение успешно обновлено.' }
        #format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        #format.json { render :json => @post.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = current_resource
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url }
      #format.json { head :no_content }
    end
  end

private

  def current_resource
    @current_resource ||= Post.find(params[:id]) if params[:id]
  end

  def success_message(post)
    if post.approved?
      'Сообщение успешно опубликовано'
    else
      'Сообщение будет опубликовано после проверки'
    end
  end

end
