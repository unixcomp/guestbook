# -*- encoding: utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  delegate :allow?, :to => :current_permission
  delegate :allow_param?, :to => :current_permission
  helper_method :current_user, :logged_in?, :authorize, :allow?, :allow_param?
  before_filter :authorize

private

  def current_user
    begin
      @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
    rescue
      cookies.delete :auth_token
      flash.now[:alert] = 'WTF!? o.O You are hacker?'
      @current_user = nil
    end
  end

  def logged_in?
     current_user != nil
  end

  def current_permission
    @current_permission ||= Permissions.permission_for(current_user)
  end

  def current_resource
    nil
  end

  def authorize
    if current_permission.allow?(params[:controller], params[:action], current_resource)
      current_permission.permit_params! params
    else
      redirect_to root_url, :alert => "Ошибка. Недостаточно прав."
    end
  end
end
